import logging
import atexit
import sqlite3
import csv

logging.basicConfig(filename='example.log',format='%(asctime)s:%(levelname)s:%(message)s',level=logging.DEBUG)

def check_table(cursor):
    cursor.execute('''CREATE TABLE IF NOT EXISTS siv(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,adresse_titulaire text,nom text,prenom text,immatriculation text,date_immatriculation text,vin text ,marque text, denomination_commerciale text, couleur text, carrosserie text,categorie text ,cylindree text, energie text, places text , poids text , puissance text , type text , variante text , version text)''')

def close_connect(connection):
    if connection:
        connection.close()

        
        
def Insertion(contain,SaveId,cursor):
        logging.debug('Insertion :'+str(contain)+" in " +str(SaveId))
        cursor.execute('INSERT INTO siv(adresse_titulaire,nom,prenom,immatriculation,date_immatriculation,vin ,marque, denomination_commerciale , couleur, carrosserie ,categorie ,cylindree , energie , places  , poids , puissance , type , variante , version ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',contain)
                        

def Update(contain,SaveId,cursor):
        logging.debug('Update :'+str(contain))
        contain.append(SaveId)
        logging.debug('A l\'indice :'+str(SaveId))
        cursor.execute("UPDATE siv SET adresse_titulaire=?,nom=?,prenom=?,immatriculation=?,date_immatriculation=?,vin=? ,marque=?, denomination_commerciale=? , couleur=?, carrosserie=? ,categorie=? ,cylindree=? , energie=? , places=?  , poids=? , puissance=? , type=? , variante=? , version=?  WHERE siv.id=?" ,contain)
                        
def remplissage():
    with open('test2.csv','r') as csvfile:
        reader=csv.reader(csvfile,delimiter=';') 

        nbligne=0
        for lines in reader:
            contain=lines
            SaveId=0
            insert=True
            
            if nbligne!= 0: 
                
                for row in cursor.execute('SELECT id, vin FROM siv '):

                        if contain[5]==row[1] :
                                logging.debug('voici le saveID : '+str(SaveId)+' voici le row 0 : '+str(row[0]))
                                logging.debug('Test:'+str(contain[5]) + "==" + str(row[1]))
                                insert=False
                                SaveId=row[0]
                        
                if insert:
                        Insertion(contain,SaveId,cursor)
                        
                else :
                        Update(contain,SaveId,cursor)
                        
            nbligne=nbligne+1
  









connection = sqlite3.connect('AutoLocaBDD.sqlite3')

cursor = connection.cursor()

atexit.register(close_connect,connection)





contain = []

check_table(cursor)

remplissage()
connection.commit()








