import unittest 
import logging
import atexit
import sqlite3
import csv
from Upsert import *
import time
class TestUpsertFunction(unittest.TestCase):

    
    def test_check_table(self):
        connection = sqlite3.connect('tests/testExist.sqlite3')
        cursor = connection.cursor()
        
        exist=False
        
        for row in cursor.execute("SELECT tbl_name FROM sqlite_master WHERE type='table'"):
            if (row[0] == "siv"):
                exist=True
        self.assertFalse(exist)

        check_table(cursor)

        for row in cursor.execute("SELECT tbl_name FROM sqlite_master WHERE type='table'"):
            if (row[0] == "siv"):
                exist=True
        self.assertTrue(exist)

        
    
    def test_Insertion(self):
        
        logging.basicConfig(filename='example.log',format='%(asctime)s:%(levelname)s:%(message)s',level=logging.DEBUG)
        connection = sqlite3.connect('tests/testInsert.sqlite3')
        cursor = connection.cursor()
        check_table(cursor)

        Insert=False

        entrer=["627 Jones Plaza Justintown, AR 10553","Cobb","Derrick","MOS 825","1948-05-04","9780574525475","Wallace, Sims and Orr","Future-proofed bottom-line matrices","LightSkyBlue","92-1185529","05-1726640","6546","28684411","38","3521","474","Group"," 42-4784392", "81011506"]
        Insertion(entrer,1,cursor)
        for row in cursor.execute("SELECT vin FROM siv "):
            if (row[0] == "9780574525475"):
                Insert=True
        self.assertTrue(Insert)

        for row in cursor.execute("SELECT vin FROM siv "):
            if (row[0] == "9780574525455"):
                Insert=False
        self.assertTrue(Insert)
        
    def test_Update(self):
       
        logging.basicConfig(filename='example.log',format='%(asctime)s:%(levelname)s:%(message)s',level=logging.DEBUG)
        connection = sqlite3.connect('tests/testUpdate.sqlite3')
        cursor = connection.cursor()
        check_table(cursor)

        Updatebool=True

        entrer=["627 Jones Plaza Justintown, AR 10553","Cobb","Derrick","MOS 825","1948-05-04","9780574525475","Wallace, Sims and Orr","Future-proofed bottom-line matrices","LightSkyBlue","92-1185529","05-1726640","6546","28684411","38","3521","474","Group"," 42-4784392", "81011506"]
        entrerupdate=["627 Stephane Plaza Black ops, AR 2","Poirot","Hercule","MOS 825","1939-11-04","9780644696469","Wallace, groomite","Future-proofed bottom-line matrixer","LukeSkywhiskey","64-1155321","05-1469434","1123","24649394","42","7894","666","moupt"," 42-8464392", "6464646"]
        Insertion(entrer,2,cursor)
        Update(entrerupdate,2,cursor)
        
        if (cursor.execute("SELECT vin FROM siv WHERE id=2").fetchone() == "9780574525475"):
            Updatebool=False
        self.assertTrue(Updatebool)

        if (cursor.execute("SELECT vin FROM siv WHERE id=2").fetchone() == "9780644696469"):

            Updatebool=True
        self.assertTrue(Updatebool)
        close_connect(connection)